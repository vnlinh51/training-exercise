import Vue from 'vue';
import Vuex from 'vuex';
import { employees, departments } from '@/data/data';

Vue.use(Vuex);

const storeData = {
    state: {
        isAuthenticated: false,
        employees,
        departments,
        name: 'Hello',
    },

    getters: {
        getAuth: (state) => state.auth,
        getEmployeeById: (state) => (id) => {
            return state.employees.filter((item) => item.id == id);
        },
        getAllEmployees: (state) => state.employees,

        getAllEmployeesByDepartmentId: (state) => (dep) => {
            if (typeof dep === 'undefined') {
                return state.employees.filter((emp) => emp.departmentId == 1);
            }
            return state.employees.filter((emp) => emp.departmentId == dep);
        },

        getAllDepartments: (state) => state.departments,
        getDepartmentNameById: (state) => (id) => {
            return state.departments.find((item) => item.departmentId == id.departmentId).name;
        },
        highestEmployees: (state) => {
            const EmployeesClone = [...state.employees];
            let top10Highest = [];
            const sortEmployeesBySalary = EmployeesClone.sort((a, b) => b.salary - a.salary);

            for (let i = 0; i < 10; i++) {
                top10Highest.push(sortEmployeesBySalary[i]);
            }
            return top10Highest;
        },
        employeesOfDepartmentHighestTotalSalary: (state) => {
            const totalSalary = [0];
            let maxIndex;

            state.employees.forEach((emp) => {
                const index = Number(emp.departmentId);
                if (!totalSalary[index]) totalSalary[index] = Number(emp.salary);
                else {
                    totalSalary[index] += Number(emp.salary);
                }
            });
            maxIndex = totalSalary.findIndex((item) => item === Math.max(...totalSalary));
            return state.employees.filter((emp) => emp.departmentId == maxIndex);
        },
    },
    actions: {
        deleteEmployee({ commit }, employeeId) {
            try {
                commit('DELETE_EMPLOYEE', employeeId);
            } catch (error) {
                console.log('Delete failed: ', error);
            }
        },
        addNewEmployee({ commit }, data) {
            commit('ADD_EMPLOYEE', data);
        },
        updateEmployee({ commit }, data) {
            commit('UPDATE_EMPLOYEE', data);
        },
    },
    mutations: {
        DELETE_EMPLOYEE(state, employeeId) {
            state.employees = state.employees.filter((emp) => emp.id !== employeeId);
        },
        ADD_EMPLOYEE(state, data) {
            state.employees.push(data);
        },
        UPDATE_EMPLOYEE(state, data) {
            const emp = state.employees.findIndex((empIndex) => empIndex.id == data.id);
            state.employees.splice(emp, 1, data);
        },
    },
};

const store = new Vuex.Store(storeData);

export default store;
