import Vue from 'vue';
import VueRouter from 'vue-router';
import MainLayout from '@/layout/MainLayout.vue';
import SingleLayout from '@/layout/SingleLayout.vue';


Vue.use(VueRouter);

export const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: () => import('../pages/Home.vue'),
            meta: { layout: MainLayout },
        },
        {
            path: '/login',
            name:'Login',
            component: () => import('../pages/Login.vue'),
            meta: { layout: SingleLayout },
        },
        {
            path: '/employee',
            name:'Employee',
            component: () => import('../pages/Employee.vue'),
            meta: { layout: MainLayout },
        },
        {
            path: '/employee/:id',
            name: 'employee_profile',
            component: () => import('../pages/DetailsEmployee.vue'),
            meta: { layout: MainLayout },
        },
        {
            path: '/departments',
            name: 'Department',
            component: () => import('../pages/Departments.vue'),
            meta: { layout: MainLayout },
        },
        {
            path: '*',
            name: '404',
            component: () => import('../pages/NotFund.vue'),
            meta: { layout: SingleLayout },
        },
    ],
});

router.beforeEach((to, from, next) => {
    if (to.name !== 'Login' && to.name !== '404' && !sessionStorage.username) next({ name: 'Login' })
    else next()
  })