export const employees = [{
    "id": 1,
    "name": "Perkin",
    "age": 23,
    "departmentId": 5,
    "salary": 3506
  }, {
    "id": 2,
    "name": "Winifred",
    "age": 26,
    "departmentId": 1,
    "salary": 2587
  }, {
    "id": 3,
    "name": "Charmine",
    "age": 21,
    "departmentId": 6,
    "salary": 2982
  }, {
    "id": 4,
    "name": "Jerry",
    "age": 36,
    "departmentId": 7,
    "salary": 1895
  }, {
    "id": 5,
    "name": "Jone",
    "age": 38,
    "departmentId": 3,
    "salary": 3253
  }, {
    "id": 6,
    "name": "Ulberto",
    "age": 21,
    "departmentId": 4,
    "salary": 2241
  }, {
    "id": 7,
    "name": "Marion",
    "age": 21,
    "departmentId": 6,
    "salary": 2357
  }, {
    "id": 8,
    "name": "Fancie",
    "age": 27,
    "departmentId": 4,
    "salary": 2801
  }, {
    "id": 9,
    "name": "Sybila",
    "age": 21,
    "departmentId": 6,
    "salary": 2423
  }, {
    "id": 10,
    "name": "Thomasine",
    "age": 27,
    "departmentId": 2,
    "salary": 3674
  }, {
    "id": 11,
    "name": "Shurlocke",
    "age": 22,
    "departmentId": 3,
    "salary": 2063
  }, {
    "id": 12,
    "name": "Cyndie",
    "age": 23,
    "departmentId": 3,
    "salary": 3051
  }, {
    "id": 13,
    "name": "Faunie",
    "age": 23,
    "departmentId": 5,
    "salary": 3115
  }, {
    "id": 14,
    "name": "Keri",
    "age": 38,
    "departmentId": 7,
    "salary": 2148
  }, {
    "id": 15,
    "name": "Jenilee",
    "age": 24,
    "departmentId": 1,
    "salary": 3508
  }, {
    "id": 16,
    "name": "Goldie",
    "age": 26,
    "departmentId": 7,
    "salary": 2287
  }, {
    "id": 17,
    "name": "Zeb",
    "age": 33,
    "departmentId": 1,
    "salary": 3900
  }, {
    "id": 18,
    "name": "Hazlett",
    "age": 28,
    "departmentId": 5,
    "salary": 2203
  }, {
    "id": 19,
    "name": "Vere",
    "age": 31,
    "departmentId": 1,
    "salary": 1847
  }, {
    "id": 20,
    "name": "Sarge",
    "age": 22,
    "departmentId": 5,
    "salary": 1599
  }, {
    "id": 21,
    "name": "Mauricio",
    "age": 25,
    "departmentId": 3,
    "salary": 1914
  }, {
    "id": 22,
    "name": "Marrilee",
    "age": 21,
    "departmentId": 7,
    "salary": 2641
  }, {
    "id": 23,
    "name": "Reginald",
    "age": 38,
    "departmentId": 2,
    "salary": 1808
  }, {
    "id": 24,
    "name": "Georgina",
    "age": 29,
    "departmentId": 1,
    "salary": 2768
  }, {
    "id": 25,
    "name": "Camellia",
    "age": 37,
    "departmentId": 2,
    "salary": 2763
  }, {
    "id": 26,
    "name": "Willdon",
    "age": 29,
    "departmentId": 5,
    "salary": 1507
  }, {
    "id": 27,
    "name": "Eugine",
    "age": 23,
    "departmentId": 7,
    "salary": 1810
  }, {
    "id": 28,
    "name": "Christina",
    "age": 29,
    "departmentId": 5,
    "salary": 2373
  }, {
    "id": 29,
    "name": "Jacquelin",
    "age": 25,
    "departmentId": 4,
    "salary": 2981
  }, {
    "id": 30,
    "name": "Tansy",
    "age": 20,
    "departmentId": 7,
    "salary": 3501
  }, {
    "id": 31,
    "name": "Nelle",
    "age": 22,
    "departmentId": 4,
    "salary": 1530
  }, {
    "id": 32,
    "name": "Avrom",
    "age": 36,
    "departmentId": 7,
    "salary": 3443
  }, {
    "id": 33,
    "name": "Lezley",
    "age": 25,
    "departmentId": 7,
    "salary": 2549
  }, {
    "id": 34,
    "name": "Brook",
    "age": 30,
    "departmentId": 6,
    "salary": 1633
  }, {
    "id": 35,
    "name": "Jamison",
    "age": 38,
    "departmentId": 2,
    "salary": 1543
  }, {
    "id": 36,
    "name": "Keeley",
    "age": 29,
    "departmentId": 7,
    "salary": 2058
  }, {
    "id": 37,
    "name": "Cornelle",
    "age": 21,
    "departmentId": 6,
    "salary": 2218
  }, {
    "id": 38,
    "name": "Hakeem",
    "age": 28,
    "departmentId": 2,
    "salary": 2641
  }, {
    "id": 39,
    "name": "Job",
    "age": 37,
    "departmentId": 4,
    "salary": 2446
  }, {
    "id": 40,
    "name": "Fayth",
    "age": 21,
    "departmentId": 7,
    "salary": 2393
  }, {
    "id": 41,
    "name": "Sigismundo",
    "age": 33,
    "departmentId": 4,
    "salary": 1771
  }, {
    "id": 42,
    "name": "Valentin",
    "age": 28,
    "departmentId": 2,
    "salary": 2347
  }, {
    "id": 43,
    "name": "Jaclyn",
    "age": 35,
    "departmentId": 5,
    "salary": 3923
  }, {
    "id": 44,
    "name": "Ancell",
    "age": 28,
    "departmentId": 5,
    "salary": 2022
  }, {
    "id": 45,
    "name": "Loy",
    "age": 24,
    "departmentId": 1,
    "salary": 1583
  }, {
    "id": 46,
    "name": "Ritchie",
    "age": 36,
    "departmentId": 3,
    "salary": 2686
  }, {
    "id": 47,
    "name": "Jeralee",
    "age": 26,
    "departmentId": 4,
    "salary": 2689
  }, {
    "id": 48,
    "name": "Ellette",
    "age": 23,
    "departmentId": 7,
    "salary": 2407
  }, {
    "id": 49,
    "name": "Christian",
    "age": 31,
    "departmentId": 6,
    "salary": 3520
  }, {
    "id": 50,
    "name": "Ephrem",
    "age": 31,
    "departmentId": 2,
    "salary": 2100
  }, {
    "id": 51,
    "name": "Courtenay",
    "age": 31,
    "departmentId": 5,
    "salary": 1870
  }, {
    "id": 52,
    "name": "Creigh",
    "age": 30,
    "departmentId": 3,
    "salary": 2141
  }, {
    "id": 53,
    "name": "Tiphani",
    "age": 23,
    "departmentId": 5,
    "salary": 1565
  }, {
    "id": 54,
    "name": "Jaclyn",
    "age": 30,
    "departmentId": 7,
    "salary": 2580
  }, {
    "id": 55,
    "name": "Pauly",
    "age": 33,
    "departmentId": 7,
    "salary": 1922
  }, {
    "id": 56,
    "name": "Adina",
    "age": 25,
    "departmentId": 6,
    "salary": 3371
  }, {
    "id": 57,
    "name": "Hedi",
    "age": 30,
    "departmentId": 5,
    "salary": 2875
  }, {
    "id": 58,
    "name": "Pierrette",
    "age": 23,
    "departmentId": 6,
    "salary": 1885
  }, {
    "id": 59,
    "name": "Ericka",
    "age": 31,
    "departmentId": 7,
    "salary": 3464
  }, {
    "id": 60,
    "name": "Filippo",
    "age": 35,
    "departmentId": 5,
    "salary": 2278
  }, {
    "id": 61,
    "name": "Ardene",
    "age": 21,
    "departmentId": 4,
    "salary": 3365
  }, {
    "id": 62,
    "name": "Deb",
    "age": 23,
    "departmentId": 4,
    "salary": 3499
  }, {
    "id": 63,
    "name": "Brennen",
    "age": 25,
    "departmentId": 7,
    "salary": 3115
  }, {
    "id": 64,
    "name": "Berte",
    "age": 23,
    "departmentId": 1,
    "salary": 2296
  }, {
    "id": 65,
    "name": "Winfred",
    "age": 34,
    "departmentId": 2,
    "salary": 3126
  }, {
    "id": 66,
    "name": "Alysa",
    "age": 34,
    "departmentId": 6,
    "salary": 3678
  }, {
    "id": 67,
    "name": "Amalea",
    "age": 35,
    "departmentId": 7,
    "salary": 2379
  }, {
    "id": 68,
    "name": "Reg",
    "age": 27,
    "departmentId": 7,
    "salary": 2245
  }, {
    "id": 69,
    "name": "Dalli",
    "age": 38,
    "departmentId": 2,
    "salary": 2110
  }, {
    "id": 70,
    "name": "Car",
    "age": 24,
    "departmentId": 5,
    "salary": 2022
  }, {
    "id": 71,
    "name": "Adeline",
    "age": 38,
    "departmentId": 6,
    "salary": 3190
  }, {
    "id": 72,
    "name": "Richie",
    "age": 23,
    "departmentId": 1,
    "salary": 3617
  }, {
    "id": 73,
    "name": "Shellie",
    "age": 21,
    "departmentId": 2,
    "salary": 5900
  }, {
    "id": 74,
    "name": "Kyrstin",
    "age": 24,
    "departmentId": 6,
    "salary": 1984
  }, {
    "id": 75,
    "name": "Keary",
    "age": 24,
    "departmentId": 6,
    "salary": 2235
  }, {
    "id": 76,
    "name": "Lenora",
    "age": 36,
    "departmentId": 3,
    "salary": 2453
  }, {
    "id": 77,
    "name": "Lilllie",
    "age": 20,
    "departmentId": 1,
    "salary": 2359
  }, {
    "id": 78,
    "name": "Sherilyn",
    "age": 38,
    "departmentId": 1,
    "salary": 1579
  }, {
    "id": 79,
    "name": "Beatrisa",
    "age": 30,
    "departmentId": 7,
    "salary": 3399
  }, {
    "id": 80,
    "name": "Bond",
    "age": 38,
    "departmentId": 5,
    "salary": 2115
  }, {
    "id": 81,
    "name": "Dulcy",
    "age": 24,
    "departmentId": 2,
    "salary": 3841
  }, {
    "id": 82,
    "name": "Erie",
    "age": 31,
    "departmentId": 6,
    "salary": 3143
  }, {
    "id": 83,
    "name": "Brunhilda",
    "age": 22,
    "departmentId": 3,
    "salary": 2486
  }, {
    "id": 84,
    "name": "Lynnelle",
    "age": 36,
    "departmentId": 6,
    "salary": 3476
  }, {
    "id": 85,
    "name": "Rhiamon",
    "age": 25,
    "departmentId": 6,
    "salary": 2115
  }, {
    "id": 86,
    "name": "Dionisio",
    "age": 23,
    "departmentId": 7,
    "salary": 1745
  }, {
    "id": 87,
    "name": "Maje",
    "age": 33,
    "departmentId": 5,
    "salary": 2199
  }, {
    "id": 88,
    "name": "Colman",
    "age": 22,
    "departmentId": 4,
    "salary": 2738
  }, {
    "id": 89,
    "name": "Sydel",
    "age": 35,
    "departmentId": 5,
    "salary": 1697
  }, {
    "id": 90,
    "name": "Catina",
    "age": 23,
    "departmentId": 7,
    "salary": 1508
  }, {
    "id": 91,
    "name": "Leopold",
    "age": 37,
    "departmentId": 6,
    "salary": 2008
  }, {
    "id": 92,
    "name": "Mozelle",
    "age": 33,
    "departmentId": 7,
    "salary": 3251
  }, {
    "id": 93,
    "name": "Cari",
    "age": 22,
    "departmentId": 2,
    "salary": 3138
  }, {
    "id": 94,
    "name": "Maxy",
    "age": 21,
    "departmentId": 2,
    "salary": 2943
  }, {
    "id": 95,
    "name": "Peggi",
    "age": 28,
    "departmentId": 2,
    "salary": 3176
  }, {
    "id": 96,
    "name": "Cherice",
    "age": 30,
    "departmentId": 5,
    "salary": 3064
  }, {
    "id": 97,
    "name": "Hodge",
    "age": 35,
    "departmentId": 2,
    "salary": 3036
  }, {
    "id": 98,
    "name": "Daffi",
    "age": 25,
    "departmentId": 3,
    "salary": 2948
  }, {
    "id": 99,
    "name": "Bunnie",
    "age": 36,
    "departmentId": 4,
    "salary": 1606
  }, {
    "id": 100,
    "name": "Joela",
    "age": 37,
    "departmentId": 2,
    "salary": 3851
  },
  {
    "id": 101,
    "name": "Michael",
    "age": 39,
    "departmentId": 6,
    "salary": 5666
  },
  {
    "id": 102,
    "name": "Luffy",
    "age": 27,
    "departmentId": 6,
    "salary": 3999
  },
  {
    "id": 103,
    "name": "Vegas",
    "age": 34,
    "departmentId": 6,
    "salary": 3666
  },
  {
    "id": 104,
    "name": "Misandry",
    "age": 30,
    "departmentId": 5,
    "salary": 5444
  },
  {
    "id": 105,
    "name": "Karen",
    "age": 31,
    "departmentId": 6,
    "salary": 5222
  },
  {
    "id": 106,
    "name": "Galileo",
    "age": 29,
    "departmentId": 7,
    "salary": 3000
  }

]



export const departments = [
    {
     name: "Marketing",
     departmentId: "1"
    },
    {
     name: "Electronics",
     departmentId: "2"
    },
    {
     name: "Clothing",
     departmentId: "3"
    },
    {
     name: "Books",
     departmentId: "4"
    },
    {
     name: "Toys",
     departmentId: "5"
    },
    {
     name: "Kids",
     departmentId: "6"
    },
    {
     name: "Computers",
     departmentId: "7"
    }
   ]

//=========================================================
// const sortFinal = (emp)=> {
//     let done=[]
//     const sortA = emp.sort((a,b) => (a.salary - b.salary)).reverse();

//      for (let index = 0; index < 10; index++) {
//          done.push(sortA[index])
//      }
//      return done
// }

// console.table(sortFinal(employees))

//=========================================================

// ====== tim phong ban co tong luong cao nhat - Cach 1 =====
// const totalSalary = [0];
// let maxIndex;
// employees.forEach((emp) => {
//     const index = Number(emp.departmentId);
//     if (!totalSalary[index]) totalSalary[index] = Number(emp.salary);
//     else {
//         totalSalary[index] += Number(emp.salary);
//     }
// });
// maxIndex = totalSalary.findIndex(item => item === Math.max(...totalSalary))

// console.log(employees.filter(emp => emp.departmentId == maxIndex))


//=========================================================

// / ====== tim phong ban co tong luong cao nhat - Cach 2 ======
// const totalSalary = [0];
// employees.forEach((emp) => {
//     const index = Number(emp.departmentId);
//     if (!totalSalary[index]) totalSalary[index] = Number(emp.salary);
//     else {
//         totalSalary[index] += Number(emp.salary);
//     }
// });

// let maxIndex = 0;
// totalSalary.forEach((item, index) => {
//     if (item > totalSalary[maxIndex]) maxIndex = index;
// });

// const c = employees.filter(emp => emp.departmentId == maxIndex)
//=========================================================




